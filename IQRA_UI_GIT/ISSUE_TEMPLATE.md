<!--
 IMPORTANT: Please use the following link to create a new issue:

  https://www.iqra.com/new-issue/iqra2

**If your issue was not created using the app above, it will be closed immediately.**
-->

<!--
Love Creative Tim? Do you need Angular, React, Vuejs or HTML? You can visit:
👉  https://www.iqra.com/bundles
👉  https://www.iqra.com
-->
