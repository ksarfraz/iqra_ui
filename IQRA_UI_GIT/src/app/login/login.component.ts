import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Countries } from 'app/constansts/countries';
import { StratusConfigService } from 'app/services/stratus-config.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isLogin = true;
  countryName;
  selectedCountry;
  countries = Countries;
  isRegister: boolean;
  isForgotPassword: boolean;
  isOTP: boolean;
  constructor(private configService: StratusConfigService, private router: Router) {
    this.selectedCountry = this.countries[0];
  }

  ngOnInit(): void {
  }
  loginSwitch(key) {
    this.isLogin = false;
    this.isRegister = false;
    this.isForgotPassword = false;
    this.isOTP = false;
    switch (key) {
      case 'login':
        this.isLogin = true;
        break;
      case 'register':
        this.isRegister = true;
        break;
      case 'forgotPassword':
        this.isForgotPassword = true;
        break;
      case 'otp':
        this.isOTP = true;
        break;
      default:
        this.isLogin = true;
        break;
    }
    
  }
  onRegSubmit(reg) {
    let values = reg.value;

    let payLoad = {
      "fullname":values.name,
      "username":values.name+values.mobileNumber,
      "password":values.password,
      "email":values.emailId,
      "mobileNumber":values.mobileNumber,
      "gender":values.gender,
      "religion":values.religion,
      "educationQalification":values.educationQualification,
      "currentOcupation":values.currentOccupation,
      "reasonForJoining":values.reasonForJoining,
      "contribution":values.yourContribution,
      "physicalAddress":"Sanjay Colony sector 23",
      "googleAddress":"Sanjay Colony sector 23 google",
      "geocordinates":"12.99999,23.456",
      "country":"India",
      "state":"Haryana",
      "city":"Faridabad",
      "locality":"Sanjay colony",
       "role":["mod","users"]
      }
    this.configService.userRegistration(payLoad).subscribe(res => {
      this.router.navigateByUrl("/dashboard");
    }, (error) => {
      if (error.status === 500) {
        console.log('User doesn’t exist in the system, please create your account.');
      } else {
        console.log('Login failed.Please try again!');
      }
    });
  }
  onForgotSubmit(reg) {
    let values = reg.value;

    let payLoad = {
      "otp":values.otp,
      "confirmPass":values.confirmPass,
      "newPass":values.newPass
      }
    this.configService.userForgotPassword(payLoad).subscribe(res => {
      this.router.navigateByUrl("/dashboard");
    }, (error) => {
      if (error.status === 500) {
        console.log('User doesn’t exist in the system, please create your account.');
      } else {
        console.log('Login failed.Please try again!');
      }
    });
  }
  onOTPSubmit(reg) {
    let values = reg.value;

    this.configService.userOTP(values.contact).subscribe(res => {
      // this.router.navigateByUrl("/dashboard");
      this.loginSwitch("forgotPassword");
    }, (error) => {
      if (error.status === 500) {
        console.log('User doesn’t exist in the system, please create your account.');
      } else {
        console.log('Login failed.Please try again!');
      }
    });
  }
  onLoginSubmit(reg) {
    let values = reg.value;

    let payLoad = {
      "username":values.userName,
      "password":values.password
      }
    this.configService.userLogin(payLoad).subscribe(res => {
      this.router.navigateByUrl("/dashboard");
    }, (error) => {
      if (error.status === 500) {
        console.log('User doesn’t exist in the system, please create your account.');
      } else {
        console.log('Login failed.Please try again!');
      }
    });
  }
}
