import { TestBed } from '@angular/core/testing';

import { StratusInitService } from './stratus-init.service';

describe('StratusInitService', () => {
  let service: StratusInitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StratusInitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
