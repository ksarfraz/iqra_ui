import { TestBed } from '@angular/core/testing';

import { StratusConfigService } from './stratus-config.service';

describe('StratusConfigService', () => {
  let service: StratusConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StratusConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
