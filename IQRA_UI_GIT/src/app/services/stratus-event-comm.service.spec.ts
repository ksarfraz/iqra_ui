import { TestBed } from '@angular/core/testing';

import { StratusEventCommService } from './stratus-event-comm.service';

describe('StratusEventCommService', () => {
  let service: StratusEventCommService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StratusEventCommService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
