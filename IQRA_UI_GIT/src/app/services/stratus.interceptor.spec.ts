import { TestBed } from '@angular/core/testing';

import { StratusInterceptor } from './stratus.interceptor';

describe('StratusInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      StratusInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: StratusInterceptor = TestBed.inject(StratusInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
