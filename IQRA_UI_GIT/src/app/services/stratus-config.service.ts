import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { environment } from '../../environments/environment';

const APIS = {
  LOGIN: `auth/signin`,
  Forgot: `auth/passChange/2`,
  REG: `auth/signup`,
  OTP: `auth/sendOTP`,
  USER_PRIVILEGES: `stratus/privileges/loggeduserprivileges`,
  USER_DETAILS: `stratus/users/profile`
};

export interface IUSERDETAILS {
  alternatephone: string;
  email: string;
  empId: string;
  gender: string;
  name: string;
  office: string;
  phone: string;
  primaryaddress: string;
  reportingmanager: string;
  team: string;
  tenantuuid: string;
  userId: string;
}
@Injectable({
  providedIn: 'root'
})
export class StratusConfigService {

  userPrivileges: string[] = [];
  userPrivilegesFetchStatus: 'DONE' | 'LOADING' | 'INIT' = 'INIT';
  firstAccessibleLink: string | undefined;
  public tenantLogo: string;
  private userPrivilegesSubject: ReplaySubject<string[]> = new ReplaySubject(1);
  constructor(
    private http: HttpClient
  ) { }
  userRegistration(user): Observable<any> {
    const url = `${environment.apiBaseUrl}/${APIS.REG}`;
    return this.http.post(url, user);
  }
  userLogin(user): Observable<any> {
    const url = `${environment.apiBaseUrl}/${APIS.LOGIN}`;
    return this.http.post(url, user);
  }
  userOTP(user): Observable<any> {
    const url = `${environment.apiBaseUrl}/${APIS.OTP}/${user}`;
    return this.http.get(url);
  }
  userForgotPassword(user): Observable<any> {
    const url = `${environment.apiBaseUrl}/${APIS.Forgot}`;
    return this.http.post(url, user);
  }
  // userRegistration() {
  //   const url = `${environment.apiBaseUrl}/${APIS.LOGIN}`;
  //   return this.http.get<{ response: string }>(url).subscribe(
  //     data => {
  //       this.tenantLogo = data.response;
  //     },
  //     _ => {
  //       console.error(_);
  //       // this.tenantLogo = CONSTANTS_STRATUS_ASSETS.IMAGES.BRAND_LOGO;
  //     }
  //   );
  // }

  getUserPrivileges(): Observable<string[]> {
    this.fetchUserPrivileges();
    return this.userPrivilegesSubject.asObservable();
  }

  private fetchUserPrivileges(): void {
    if (this.userPrivileges.length || this.userPrivilegesFetchStatus === 'LOADING') {
      return;
    }
    this.userPrivilegesFetchStatus = 'LOADING';
    const url = `${environment.apiBaseUrl}/${APIS.USER_PRIVILEGES}`;
    this.http.get<string[]>(url).subscribe(
      res => {
        /**The below code is used only for development time so as to
         * simulate all the features on the localhost. It will never be enabled in build code
         */
        this.userPrivileges = res || [];
        // Evaluating accessible links for current user
        this.userPrivilegesFetchStatus = 'DONE';
        this.userPrivilegesSubject.next(res);
      },
      _ => {
        this.userPrivilegesFetchStatus = 'DONE';
        this.userPrivilegesSubject.next([]);
      }
    );
  }

  // tslint:disable-next-line: no-any
  // updateLogo(reqFile: FormData): Observable<any> {
  //   const url = `${environment.apiBaseUrl}/${APIS.LOGO}`;
  //   return this.http.post<{ response: string }>(url, reqFile);
  // }


}
