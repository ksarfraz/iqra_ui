import { Injectable } from '@angular/core';

// tslint:disable-next-line: ban-types
declare let ga: Function; // Declare ga as a function
// tslint:disable-next-line: no-any
declare let require: any;
const amplitude = require('amplitude-js');
@Injectable({
  providedIn: 'root'
})
export class StratusInitService {

  constructor(

  ) {
    const token = this.getUrlQueryParams('authToken');
    // Do not change the order here
  }

  initApp(): Promise<void> {
    console.info('STRATUS INIT.');
    return new Promise<void>((resolve, reject) => {
      setTimeout(() => {
        const amplitudeAnalytics = require('amplitude-js');
        amplitudeAnalytics.getInstance().init('a65d2d8213dda472e1711f2925fb509b');
        console.info('AMPLITUDE KEY INITIALIZED');
        resolve();
      }, 0);

    });

  }

  private getUrlQueryParams(queryString: string): string | null {
    const results = new RegExp('[?&]' + queryString + '=([^&#]*)').exec(
      window.location.href
    );
    if (results === null) {
      return null;
    } else {
      return decodeURIComponent(results[1]) || null;
    }
  }

  // tslint:disable-next-line: typedef
  public analyticsEmitter(eventCategory: string, eventAction: string, eventLabel: string, eventValue?: number) {
    ga('send', 'event', {
      eventCategory,
      eventAction,
      eventLabel,
      eventValue
    });

    amplitude.getInstance().logEvent(eventCategory,
      {
        eventValue: eventAction,
        eventPage: eventLabel
      });
  }
}
