import {
  HttpErrorResponse, HttpEvent, HttpHandler,
  HttpInterceptor, HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, timeout } from 'rxjs/operators';

@Injectable()
export class StratusInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const removeDefaultTokenHeader = request.headers.get('removeDefaultToken') || 'n';
    const timeoutValue = request.headers.get('timeout') || (location.hostname === 'localhost' ? 60000 : 45000);
    const timeoutValueNumeric = Number(timeoutValue);
    let newHeaders = request.headers;
    if (removeDefaultTokenHeader.toLowerCase() === 'n') {
      newHeaders = newHeaders.append('Authorization', `Bearer `);
    }
    newHeaders = newHeaders.delete('timeout');
    newHeaders = newHeaders.delete('removeDefaultToken');
    const cloneRequest = request.clone({ headers: newHeaders });
    return this.executeRequest(cloneRequest, next, timeoutValueNumeric);
  }

  private executeRequest(request: HttpRequest<unknown>, next: HttpHandler, timeOutVal: number): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      tap(
        _ => { },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 403 && (err.url?.includes(location.origin) || location.origin.includes('localhost'))) {
              console.warn('403 detected for the resource: ', request.url);
              // if (this.authSvc.userDetails.exp < Date.now() / 1000) {
                console.error('Token expired. Logging out');
              //   UIkit.notification({
              //     message: '<div class="uk-text-warning uk-text-bold">Sorry for the interruption but seems like your session has expired.</div>',
              //   // });
              //   // this.authSvc.logout();
              // }
            }
          }
        }
      ),
      timeout(timeOutVal)
    );
  }
}
