import { Injectable } from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';

const VALID_APPS = ['EMPLOYEE-MANAGEMENT', 'TEAM-PLANNER'];
type TEventType = 'INIT' | 'INIT_ACKNOWLEDGED' | 'LOADED';
interface ICommEvent {
  app: string;
  eventType: TEventType;
  // tslint:disable-next-line: no-any
  data: any | null;
}

@Injectable({
  providedIn: 'root'
})
export class StratusEventCommService {

  inviteEmployeeSubject = new Subject();
  private stratusEvents$ = fromEvent<MessageEvent>(window, 'message').pipe(
    filter(
      (messageEvent: MessageEvent) =>
        messageEvent.data &&
        messageEvent.data.app &&
        typeof messageEvent.data.app === 'string' &&
        VALID_APPS.includes(messageEvent.data.app)
    )
  );

  constructor() {
  }

  handleEvents(): void {
    this.stratusEvents$.subscribe(
      (event: MessageEvent) => {
        console.info('INPUT MESSAGE EVENT >', event);
        const receivedEventData = event.data as ICommEvent;
        if (receivedEventData.eventType === 'INIT') {
          this.fireEvents(null, (event.source as Window), event.origin, 'INIT_ACKNOWLEDGED');
        } else if (receivedEventData.eventType === 'LOADED') {
          if (receivedEventData.data) {
            const concentratedData = receivedEventData.data;
            if (concentratedData.action === 'INVITE_EMPLOYEE') {
              this.inviteEmployeeSubject.next();
            }
          }
        }
      }
    );
  }

  // tslint:disable-next-line: no-any
  fireEvents(data: any | null, contentWindow: Window, origin: string, eventType: TEventType): void {
    const dataTransmit: ICommEvent = {
      app: 'STRATUS',
      data,
      eventType
    };
    contentWindow.postMessage(dataTransmit, origin);
  }

}
